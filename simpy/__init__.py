# Declare a namespace for the simpy package. This file is not included in the
# distribution and only required for development installations.

from pkgutil import extend_path

__path__ = extend_path(__path__, __name__)
