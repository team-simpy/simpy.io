import pytest

from simpy.io import virtual


@pytest.fixture()
def env(request):
    return virtual.Environment()


@pytest.fixture()
def link_type(env, request):
    return virtual.TCPSocket
