import pytest

from simpy.io import async


@pytest.fixture()
def env(request):
    env = async.Environment()
    request.addfinalizer(env.close)
    return env


@pytest.fixture()
def link_type(env, request):
    return async.TCPSocket
