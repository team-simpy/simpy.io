import pytest

from simpy.io import select
from simpy.io.test.pipe import *


@pytest.fixture()
def env(request):
    env = select.Environment()
    request.addfinalizer(env.close)
    return env


@pytest.fixture()
def pipe_type(request):
    return select.Pipe
