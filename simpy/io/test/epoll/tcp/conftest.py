import pytest

from simpy.io import poll


@pytest.fixture()
def env(request):
    env = poll.Environment(type='epoll')
    request.addfinalizer(env.close)
    return env


@pytest.fixture()
def link_type(env, request):
    return poll.TCPSocket
