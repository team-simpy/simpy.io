import time

from simpy.core import Environment
from simpy.io import virtual, async, select
from simpy.io.codec import JSON
from simpy.io.packet import PacketUTF8
from simpy.io.message import Message


backends = {
    'virtual': {'env': virtual.Environment, 'socket': virtual.TCPSocket},
    'async': {'env': async.Environment, 'socket': async.TCPSocket},
    'select': {'env': select.Environment, 'socket': select.TCPSocket},
    'poll': {'env': select.Environment, 'socket': select.TCPSocket},
}


def run(backend, duration, payload):
    env = backends[backend]['env']()
    socket = backends[backend]['socket']
    service = socket.server(env, ('127.0.0.1', 0))

    def server(env, service):
        while True:
            conn = yield service.accept()
            message = Message(env, PacketUTF8(conn), JSON())
            env.process(echo(message, 'server'))

    def echo(message, name):
        while True:
            req = yield message.recv()
            req.succeed()

    json = JSON()

    env.process(server(env, service))
    conn = socket.connection(env, service.address)
    client = Message(env, PacketUTF8(conn), JSON())

    message_count = [0]

    def send_data(client, payload):
        while True:
            yield client.send(payload)
            message_count[0] += 1

    payload = ' ' * payload
    proc = env.process(send_data(client, payload))
    process_time = time.time()
    while time.time() - process_time < duration:
        env.step()

    if hasattr(env, 'close'):
        env.close()

    print('%s: %.2f message per second' % (backend, message_count[0] /
        duration))



for backend in sorted(backends):
    run(backend, 3, 10)
